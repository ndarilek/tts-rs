# TTS-RS

This library provides a high-level Text-To-Speech (TTS) interface supporting various backends. Currently supported backends are:

 * [Speech Dispatcher](https://freebsoft.org/speechd) (Linux)
 * WebAssembly
